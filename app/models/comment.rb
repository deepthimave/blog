class Comment < ActiveRecord::Base
	#To add assoviations between post and comments
	belongs_to :post
	#Validates the comment
	validates_presence_of :post_id
	validates_presence_of :body
end
